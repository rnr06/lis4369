import datetime
import pandas_datareader as pdr # remote data access for pandas
import matplotlib.pyplot as plt
from matplotlib import style

#Developer: Rhianna Reichert

# Course: LIS4369

# Semester: Fall 2020

# Program requirements:
def get_requirements():
    print("Data Analysis 1")
    print("Developer: Rhianna Reichert")
    print("\nProgram Requirements:\n"
    + "1. Run demo.py.\n"
    + "2. If errors, more than likely missing installations.\n"
    + "3. Test Python Package Installer: pip freeze.\n"
    + "4. Research how to do the following installations:.\n"
    + "\ta. pandas (only if missing).\n"
    + "\tb. pandas-datareader (only if missing).\n"
    + "\tc. matplotlib (only if missing).\n"
    + "\te. print_painting_percentage(): displays painting costs percentages.\n"
    + "5. Format, right-align numbers, and round to two decimal places.\n"
    + "\ta. main(): calls at least two other functions.\n"
    + "\tb. get_requirements(): displays the program requirements.\n"
    + "\tc. data_analysis_1(): displays the following data.\n")

def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print("\nPrint columns: ")
    print(df.columns)








    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines:")
    # Note: "Date" is lower than the other columns as it is treated as an index
    print(df.head())

    print("\nPrint last five lines:")
    print(df.tail())

    print("\nPrint first 2 lines:")
    print(df.head(2))

    print("\nPrint last 2 lines:")
    print(df.tail(2))




    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()