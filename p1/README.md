> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
# (Python/R Data Analytics/Visualization)

## Rhianna Reichert

### Project 1 Requirements:

*Five parts:*

1. Backward-engineer (using Python) demo.py program.
2. Test Python Package Installer.
3. Research how to do the following installations:
    - a. pandas (only if missing)
    - b. pandas-datareader (only if missing)
    - c. matplotlib (only if missing)
4. Create at least three functions that are called by the program:
    - a. main(): calls at least two other functions.
    - b. get_requirements(): displays the program requirements.
    - c. data_analysis_1(): displays the data.
5. Test the program using both IDLE and Visual Studio Code.

#### README.md file should include the following items:

* Screenshot of running [demo.py](p1_data_analysis_1/demo.py "demo.py")
* Jupyter Notebook of [p1_data_analysis_1.ipynb](p1_data_analysis_1/p1_data_analysis_1.ipynb "p1_data_analysis_1.ipynb")
* Screenshot of Python skill sets
    - Skill Set 7: Using Lists
    - Skill Set 8: Using Tuples
    - Skill Set 9: Using Sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Data Analysis 1 App*:

|       Data Analysis 1 App - 1         |            Data Analysis 1 App - 2             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Data Analysis 1 App - 1](img/p1_data_analysis_1_1.png "Data Analysis 1 App - 1")              |                    ![Data Analysis 1 App - 2](img/p1_data_analysis_1_2.png "Data Analysis 1 App - 2")

![Data Analysis 1 App - Graph](img/p1_data_analysis_1_graph.png "Data Analysis 1 App - Graph")


*Screenshots of [Jupyter Notebook - Data Analysis 1 App](p1_data_analysis_1/p1_data_analysis_1.ipynb "Jupyter Notebook - Data Analysis 1 App")*:

|        Screenshot 1: Jupyter Notebook - Data Analysis 1 App        |       Screenshot 2: Jupyter Notebook - Data Analysis 1 App   |        Screenshot 3: Jupyter Notebook - Data Analysis 1 App                                     
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot 1: Jupyter Notebook - Data Analysis 1 App](img/jupyter_notebook_1.png "Screenshot 1: Jupyter Notebook - Data Analysis 1 App")              |                    ![Screenshot 2: Jupyter Notebook - Data Analysis 1 App](img/jupyter_notebook_2.png "Screenshot 2: Jupyter Notebook - Data Analysis 1 App")|                    ![Screenshot 3: Jupyter Notebook - Data Analysis 1 App](img/jupyter_notebook_3.png "Screenshot 3: Jupyter Notebook - Data Analysis 1 App")


*Screenshots of Python Skill Sets*:

|        Skill Set 7: Using Lists        |       Skill Set 8: Using Tuples   |
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 7: Using Lists](img/ss7_python_lists.png "Skill Set 7: Using Lists")              |                    ![Skill Set 8: Using Tuples](img/ss8_using_tuples.png "Skill Set 8: Using Tuples")|

*Screenshots of Python Skill Sets Continued*:

|       Skill Set 9: Using Sets - 1   |               Skill Set 9: Using Sets - 2   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 9: Using Sets - 1](img/ss9_using_sets_1.png "Skill Set 9: Using Sets - 1")|              ![Skill Set 9: Using Sets - 2](img/ss9_using_sets_2.png "Skill Set 9: Using Sets - 2")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")