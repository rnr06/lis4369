> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
# (Python/R Data Analytics/Visualization)

## Rhianna Reichert

### Assignment 5 Requirements:

*Three parts:*

1. Complete the tutorial Introduction_to_R_Setup_and_Tutorial.
2. Code and run lis4369_a5.R
    - Include *at least* two plots.
3. Test the program using RStudio

#### README.md file should include the following items:

* Screenshots of [Introduction_to_R_Setup_and_Tutorial](r_tutorial/learn_to_use_r.R "Introduction_to_R_Setup_and_Tutorial")
* Screenshots of [lis4369_a5.R](lis4369_a5.R "lis4369_a5.R") output
* Screenshot of Python skill sets
    - Skill Set 13: Sphere Volume Calculator
    - Skill Set 14: Calculator with Error Handling
    - Skill Set 15: File Write/Read

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Introduction_to_R_Setup_and_Tutorial*:

![Introduction_to_R_Setup_and_Tutorial](img/Rstudio_tutorial.png "Introduction_to_R_Setup_and_Tutorial")

|       Introduction_to_R_Setup_and_Tutorial - Graph 1         |            Introduction_to_R_Setup_and_Tutorial - Graph 2             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Introduction_to_R_Setup_and_Tutorial - Graph 1](img/Rstudio_tutorial_graph_1.png "Introduction_to_R_Setup_and_Tutorial - Graph 1")              |                    ![Introduction_to_R_Setup_and_Tutorial - Graph 2](img/Rstudio_tutorial_graph_2.png "Introduction_to_R_Setup_and_Tutorial - Graph 2")


*Screenshots of LIS4369_A5.R*:

|       LIS4369_A5.R demo1.R         |            LIS4369_A5.R demo2.R             |
|:---------------------------------:|:--------------------------------------------:|
|              ![LIS4369_A5.R demo1.R](img/a5_demo1.png "LIS4369_A5.R demo1.R")              |                    ![LIS4369_A5.R demo2.R](img/a5_demo2.png "LIS4369_A5.R demo2.R")

*Screenshot of LIS4369_A5.R Final Output*:

![LIS4369_A5.R Final Output](img/a5_final_output.png "LIS4369_A5.R Final Output")


*Screenshots of Python Skill Sets*:

|        Skill Set 13: Sphere Volume Calculator        |       Skill Set 14: Calculator with Error Handling - 1   |
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 13: Sphere Volume Calculator](img/ss13_sphere_volume_calculator.png "Skill Set 13: Sphere Volume Calculator")              |                    ![Skill Set 14: Calculator with Error Handling - 1](img/ss14_calculator_with_error_handling_1.png "Skill Set 14: Calculator with Error Handling - 1")|

*Screenshots of Python Skill Sets Continued*:

|       Skill Set 14: Calculator with Error Handling - 2   |               Skill Set 15: File Write/Read   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 14: Calculator with Error Handling - 2](img/ss14_calculator_with_error_handling_2.png "Skill Set 14: Calculator with Error Handling - 2")|              ![Skill Set 15: File Write/Read](img/ss15_file_write_read.png "Skill Set 15: File Write/Read")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")