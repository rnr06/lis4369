> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
# (Python/R Data Analytics/Visualization)

## Rhianna Reichert

### Assignment 4 Requirements:

*Five parts:*

1. Backward-engineer (using Python) demo.py program.
2. Test Python Package Installer.
3. Research how to do the following installations:
    - a. pandas (only if missing)
    - b. pandas-datareader (only if missing)
    - c. matplotlib (only if missing)
4. Create at least three functions that are called by the program:
    - a. main(): calls at least two other functions.
    - b. get_requirements(): displays the program requirements.
    - c. data_analysis_2(): displays the data.
5. Test the program using both IDLE and Visual Studio Code.

#### README.md file should include the following items:

* Screenshot of running [demo.py](a4_data_analysis_2/demo.py "demo.py")
* Jupyter Notebook of [a4_data_analysis_2.ipynb](a4_data_analysis_2/a4_data_analysis_2.ipynb "a4_data_analysis_2.ipynb")
* Screenshot of Python skill sets
    - Skill Set 10: Python Dictionaries
    - Skill Set 11: Random Number Generator
    - Skill Set 12: Temperature Conversion Program

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Data Analysis 2 App*:

|       Data Analysis 2 App - 1         |            Data Analysis 2 App - 2             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Data Analysis 2 App - 1](img/a4_data_analysis_2_1.png "Data Analysis 2 App - 1")              |                    ![Data Analysis 2 App - 2](img/a4_data_analysis_2_2.png "Data Analysis 2 App - 2")

![Data Analysis 2 App - Graph](img/a4_data_analysis_2_graph.png "Data Analysis 2 App - Graph")


*Screenshots of [Jupyter Notebook - Data Analysis 2 App](a4_data_analysis_2/a4_data_analysis_2.ipynb "Jupyter Notebook - Data Analysis 1 App")*:

|        Screenshot 1: Jupyter Notebook - Data Analysis 2 App        |       Screenshot 2: Jupyter Notebook - Data Analysis 2 App   |        Screenshot 3: Jupyter Notebook - Data Analysis 2 App                                     
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot 1: Jupyter Notebook - Data Analysis 2 App](img/jupyter_notebook_1.png "Screenshot 1: Jupyter Notebook - Data Analysis 2 App")              |                    ![Screenshot 2: Jupyter Notebook - Data Analysis 2 App](img/jupyter_notebook_2.png "Screenshot 2: Jupyter Notebook - Data Analysis 2 App")|                    ![Screenshot 3: Jupyter Notebook - Data Analysis 2 App](img/jupyter_notebook_3.png "Screenshot 3: Jupyter Notebook - Data Analysis 2 App")


*Screenshots of Python Skill Sets*:

|        Skill Set 10: Python Dictionaries - 1        |       Skill Set 10: Python Dictionaries - 2   |
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 10: Using Dictionaries - 1](img/ss10_python_dictionaries_1.png "Skill Set 10: Using Dictionaries - 1")              |                    ![Skill Set 10: Using Dictionaries - 2](img/ss10_python_dictionaries_2.png "Skill Set 10: Using Dictionaries - 2")|

*Screenshots of Python Skill Sets Continued*:

|       Skill Set 11: Random Number Generator   |               Skill Set 12: Temperature Conversion Program   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 11: Random Number Generator](img/ss11_random_number_generator.png "Skill Set 11: Random Number Generator")|              ![Skill Set 12: Temperature Conversion Program](img/ss12_temperature_conversion_program.png "Skill Set 12: Temperature Conversion Program")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")