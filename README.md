> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
# (Python/R Data Analytics/Visualization)

## Rhianna Reichert

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create a1_tip_calculator application
    - Create a1 tip calculator Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Payroll app using "Separation of Concerns" design principles
    - Provide screenshots of completed app
    - Provide screenshots of completed Python skill sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Backward-engineer (using Python) Painting Estimator program.
    - The program should be organized with two modules:
        - a. functions.py module contains the following functions:
            - i. get_requirements()
            - ii. estimate_painting_cost()
            - iii. print_painting_estimate()
            - iv. print_painting_percentage()
        - b. main.py module imports the functions.py module, and calls the functions.
    - Test the program using both IDLE and Visual Studio Code.

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Backward-engineer (using Python) demo.py program.
    - Test Python Package Installer.
    - Research how to do the following installations:
        - a. pandas (only if missing)
        - b. pandas-datareader (only if missing)
        - c. matplotlib (only if missing)
    - Create at least three functions that are called by the program:
        - a. main(): calls at least two other functions.
        - b. get_requirements(): displays the program requirements.
        - c. data_analysis_2(): displays the data.
    - Test the program using both IDLE and Visual Studio Code.
    - Screenshot of Python skill sets
        - Skill Set 10: Python Dictionaries
        - Skill Set 11: Random Number Generator
        - Skill Set 12: Temperature Conversion Program

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshots of [Introduction_to_R_Setup_and_Tutorial](a5/r_tutorial/learn_to_use_r.R "Introduction_to_R_Setup_and_Tutorial")
    - Screenshots of [lis4369_a5.R](a5/lis4369_a5.R "lis4369_a5.R") output
    - Screenshot of Python skill sets
        - Skill Set 13: Sphere Volume Calculator
        - Skill Set 14: Calculator with Error Handling
        - Skill Set 15: File Write/Read

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Backward-engineer (using Python) demo.py program.
    - Test Python Package Installer.
    - Research how to do the following installations:
        - a. pandas (only if missing)
        - b. pandas-datareader (only if missing)
        - c. matplotlib (only if missing)
    - Create at least three functions that are called by the program:
        - a. main(): calls at least two other functions.
        - b. get_requirements(): displays the program requirements.
        - c. data_analysis_1(): displays the data.
    - Test the program using both IDLE and Visual Studio Code.

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Screenshots of [lis4369_p2.R](p2/lis4369_p2.R "lis4369_p2.R") output
    - Screenshots of at least two plots from P2 file.


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
