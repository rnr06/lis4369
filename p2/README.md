> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
# (Python/R Data Analytics/Visualization)

## Rhianna Reichert

### Project 2 Requirements:

*Three parts:*

1. Backward-engineer the lis4369_p2_requirements.txt file.
2. Code and run lis4369_p2.R
    - Include *at least* two plots.
3. Test the program using RStudio

#### README.md file should include the following items:

* Screenshots of [lis4369_p2.R](lis4369_p2.R "lis4369_p2.R") output
* Screenshots of at least two plots from P2 file.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of LIS4369_P2.R*:

![LIS4369_P2.R Running](p2_running.png "LIS4369_P2.R Running")


*Screenshots of LIS4369_P2.R Plots*:

|       LIS4369_P2.R - Displacement vs MPG plot         |            LIS4369_P2.R - Weight in Thousands plot             |
|:---------------------------------:|:--------------------------------------------:|
|              ![LIS4369_P2.R - Displacement vs MPG plot](plot_disp_and_mpg_1.png "LIS4369_P2.R - Displacement vs MPG plot")              |                    ![LIS4369_P2.R - Weight in Thousands plot](plot_disp_and_mpg_2.png "LIS4369_P2.R - Weight in Thousands plot")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")