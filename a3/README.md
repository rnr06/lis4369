> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
# (Python/R Data Analytics/Visualization)

## Rhianna Reichert

### Assignment 3 Requirements:

*Three parts:*

1. Backward-engineer (using Python) Painting Estimator program.
2. The program should be organized with two modules:
    - a. functions.py module contains the following functions:
        - i. get_requirements()
        - ii. estimate_painting_cost()
        - iii. print_painting_estimate()
        - iv. print_painting_percentage()
    - b. main.py module imports the functions.py module, and calls the functions.
3. Test the program using borth IDLE and Visual Studio Code.

#### README.md file should include the following items:

* Screenshot of running painting estimator app
* Screenshot of Python skill sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Painting Estimator App*:

|       Painting Estimator - 1         |            Painting Estimator - 2             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Painting Estimator - 1](img/a3_painting_estimator_1.png "Painting Estimator - ")              |                    ![Painting Estimator - 2](img/a3_painting_estimator_2.png "Painting Estimator - 2")

*Screenshots of [Jupyter Notebook - Painting Estimator App](a3_painting_estimator/a3_painting_estimator.ipynb "Jupyter Notebook - Payroll App")*:

|        Screenshot 1: Jupyter Notebook - Painting Estimator App - 1        |       Screenshot 2: Jupyter Notebook - Painting Estimator App - 2   |               Screenshot 3: Jupyter Notebook - Painting Estimator App - 3   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Screenshot 1: Jupyter Notebook - Painting Estimator App](img/jupyter_notebook_1.png "Screenshot 1: Jupyter Notebook - Painting Estimator Apps")              |                    ![Screenshot 2: Jupyter Notebook - Painting Estimator App](img/jupyter_notebook_2.png "Screenshot 2: Jupyter Notebook - Painting Estimator App")|              ![Screenshot 3: Jupyter Notebook - Painting Estimator App](img/jupyter_notebook_3.png "Screenshot 3: Jupyter Notebook - Painting Estimator App")

*Screenshots of Python Skill Sets*:

|        Skill Set 4: Calorie Percentage        |       Skill Set 5: Python Selection Structures - 1   |               Skill Set 5: Python Selection Structures - 2   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Skill Set 4: Calorie Percentage](img/ss4_calorie_percentage.png "Skill Set 4: Calorie Percentage")              |                    ![Skill Set 5: Python Selection Structures - 1](img/ss5_python_selection_structures_1.png "Skill Set 5: Python Selection Structures - 1")|              ![Skill Set 5: Python Selection Structures - 2](img/ss5_python_selection_structures_2.png "Skill Set 5: Python Selection Structures - 2"))

*Screenshots of Python Skill Sets Continued*:

|       Skill Set 6: Python Selection Loops - 1   |               Skill Set 6: Python Selection Loops - 2   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|                    ![Skill Set 6: Python Looping Structures](img/ss6_python_looping_structures_1.png "Skill Set 6: Python Looping Structures")|              ![Skill Set 6: Python Looping Structures](img/ss6_python_looping_structures_2.png "Skill Set 6: Python Looping Structures")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")