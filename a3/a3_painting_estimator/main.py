import functions as f


def main():
    # See Murach Python: Ch. 4 p. 121
    f.get_requirements()
    check = "y"
    while check.lower() == "y":
        f.estimate_painting_cost()
        print()
        check = input("Estimate another paint job? (y/n): ")
        print()

    print("Thank you for using our Painting Estimator!")
    print("Please see our web site: http:/www.mysite.com")


# test  whether script is being run directly, or being imported by something else
# https://stackoverflow.com/questions/419163/what-does-if-name-main-do
if __name__ == "__main__":
    main()