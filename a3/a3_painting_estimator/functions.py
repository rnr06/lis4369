#!/usr/bin/env python3

#Developer: Rhianna Reichert

# Course: LIS4369

# Semester: Fall 2020

# Painting notes
# Cost per gallon ($30): https://www.paintzen.com/advice/what-does-paint-cost-per-gallon
# SQ FT per gallon regular (350) (avg 250-400): https://www.lowes.com/cd_Paint+Calculator_1352225126183
# SQ FT per gallon primer (200): https://www.lowes.com/cd_Paint+Calculator_1352225126183
# Cost to paint home interior ($2/sq ft): https://smartasset.com/mortgage/the-average-cost-to-paint-a-house

"""Defines four funcions:

1. get_requirements()
2. estimate_painting_cost()
3. print_painting_estimate()
4. print_painting_percentage()
"""

# Program requirements:
def get_requirements():
    print("Developer: Rhianna Reichert")
    print("Painting Estimator")
    print("\nProgram Requirements:\n"
    + "1. Calculate home interior paint cost (w/o primer).\n"
    + "2. Must use float data types.\n"
    + "3. Must use SQFT_PER_GALLON constant (350).\n"
    + "4. Must use iteration structure (aka \"loop\").\n"
    + "5. Format, right-align numbers, and round to two decimal places.\n"
    + "6. Create at least five functions that are called by the program:\n"
    + "\ta. main(): calls two other functions: get_requirements() and estimate(painting_cost().\n"
    + "\tb. get_requirements(): displays the program requirements.\n"
    + "\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n"
    + "\td. print_painting_estimate(): displays painting costs.\n"
    + "\te. print_painting_percentage(): displays painting costs percentages.\n")

def estimate_painting_cost():
    # initialize variables
    # 350 sq. ft. per gallon (not including primer)
    SQFT_PER_GALLON = 350
    total_sqft = 0.0

    # IPO: Input > Process > Output
    # get user data
    print("Input:")
    total_sqft = float(input("Enter total interior sq ft: "))
    price_per_gallon = float(input("Enter price per gallon paint: "))   # $30
    painting_rate = float(input("Enter hourly painting rate per sq ft: "))  # $2

    # Process:
    # calculations
    paint_gallons = total_sqft / SQFT_PER_GALLON
    paint_cost = paint_gallons * price_per_gallon
    labor_cost = total_sqft * painting_rate
    total_cost = paint_cost + labor_cost
    paint_percent = paint_cost / total_cost
    labor_percent = labor_cost / total_cost

    # display painting estimate
    print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, price_per_gallon, painting_rate)

    # display painting percentage
    print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total_cost)

    # Output:
    # https://docs.python.olrg/3/library/string.html#format-specification-mini-language
    # https://docs.python.olrg/3/library/string.html#string-formatting
    # https://www.python-course.eu/python3_formatted_output.php
    # https://www.digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3

    # Note: % automatically multiplies by 100 followed by %


def print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, price_per_gallon, painting_rate):
    print("\nOutput:")
    print("{0:20} {1:>9}".format("Item", "Amount"))
    print("{0:20} {1:9,.2f}".format("Total Sq Ft:", total_sqft))
    print("{0:20} {1:9,.2f}".format("Sq Ft per Gallon:", SQFT_PER_GALLON))
    print("{0:20} {1:9,.2f}".format("Number of Gallons:", paint_gallons))
    print("{0:20} ${1:8,.2f}".format("Paint per Gallon:", price_per_gallon))
    print("{0:20} ${1:8,.2f}".format("Labor per Sq Ft:", painting_rate))

    print()

def print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total_cost):
    print("\nOutput:")
    print("{0:8} {1:>9} {2:>13}".format("Cost", "Amount", "Percentage"))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Paint:", paint_cost, paint_percent))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Labor:", labor_cost, labor_percent))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Total:", total_cost, paint_percent + labor_percent))