> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
# (Python/R Data Analytics/Visualization)

## Rhianna Reichert

### Assignment 2 Requirements:

*Three parts:*

1. Payroll app
2. Chapter Questions (Chs 3, 4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running payroll app
* Screenshots of Python skill sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Payroll App*:

|       Payroll No Overtime         |            Payroll with Overtime             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Payroll No Overtime](img/payroll_no_overtime.png "Payroll No Overtime")              |                    ![Payroll with Overtime](img/payroll_with_overtime.png "Payroll with Overtime")

*Screenshots of [Jupyter Notebook - Payroll App](a2_payroll/a2_payroll.ipynb "Jupyter Notebook - Payroll App")*:

|        Screenshot 1: Jupyter Notebook - Payroll App        |       Screenshot 2: Jupyter Notebook - Payroll App   |               Screenshot 3: Jupyter Notebook - Payroll App   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Screenshot 1: Jupyter Notebook - Payroll App](img/jupyter_notebook_1.png "Screenshot 1: Jupyter Notebook - Payroll Apps")              |                    ![Screenshot 2: Jupyter Notebook - Payroll App](img/jupyter_notebook_2.png "Screenshot 2: Jupyter Notebook - Payroll App")|              ![Screenshot 3: Jupyter Notebook - Payroll App](img/jupyter_notebook_3.png "Screenshot 3: Jupyter Notebook - Payroll App")

*Screenshots of Python Skill Sets*:

|        Skill Set 1: Square Feet to Acres        |       Skill Set 2: Miles Per Gallon   |               Skill Set 3: IT/ICT Student Percentage   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Skill Set 1: Square Feet to Acres](img/ss1_square_feet_to_acres.png "Skill Set 1: Square Feet to Acres")              |                    ![Skill Set 2: Miles Per Gallon](img/ss2_miles_per_gallon.png "Skill Set 2: Miles Per Gallon")|              ![Skill Set 3: IT/ICT Student Percentage](img/ss3_it_ict_student_percentage.png "Skill Set 3: IT/ICT Student Percentage")              





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")