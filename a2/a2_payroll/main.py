import functions as f


def main():
    f.get_requirements()
    # f.user_input()
    f.calculate_payroll()

if __name__ == "__main__":
    main()