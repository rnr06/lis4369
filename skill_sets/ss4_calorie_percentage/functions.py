def get_requirements():
    print("Developer: Rhianna Reichert")
    print("Calorie Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find calories per grams of fat, carbs, and protein.\n"
        + "2. Calculate percentages.\n"
        + "3. Must use float data types.\n"
        + "4. Format, right-align numbers, and round to two decimal places.\n")


def calculate_calorie_percentage():
    # initialize variables
    fat_constant = (9)
    carb_constant = (4)
    protein_constant = (4)

    # IPO: Input > Process > Output
    # get user data
    print("Input:")
    fat = float(input("Enter total fat grams: "))
    carb = float(input("Enter total carb grams: "))
    protein = float(input("Enter total protein grams: "))

    # Process:
    # calculate information
    fat_total = fat * fat_constant
    carb_total = carb * carb_constant
    protein_total = protein * protein_constant

    fat_percentage = fat_total / (fat_total + carb_total + protein_total)
    carb_percentage = carb_total / (fat_total + carb_total + protein_total)
    protein_percentage = protein_total / (fat_total + carb_total + protein_total)

    # print output
    # https://docs.python.org/3/library/string.html#format-specification-mini-language
    # https://docs.python.org/3/library/string.html#string-formatting
    # https://www.python-course.eu/python3_formatted_output.php
    # https://www.digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3

    # display results
    print("\nOutput:")
    print("Type\t" + "\t" + "Calories" + "    Percentage")
    print("Fat\t" + "\t{:.2f}".format(fat_total) + "\t" + "\t{:.2%}".format(fat_percentage))
    print("Carbs\t" + "\t{:.2f}".format(carb_total) + "\t" + "\t{:.2%}".format(carb_percentage))
    print("Protein\t" + "\t{:.2f}".format(protein_total) + "\t" + "\t{:.2%}".format(protein_percentage))
    print("")