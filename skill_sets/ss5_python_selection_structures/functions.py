def get_requirements():
    print("Developer: Rhianna Reichert")
    print("Python Selection Structures")
    print("\nProgram Requirements:\n"
        + "1. Use Python selection structure.\n"
        + "2. Prompt user for two numbers, and a suitable operator.\n"
        + "3. Test for correct numeric operator.\n"
        + "4. Replicate display below.\n")


def print_selection_structures():
    # selection structures

    # if...else if...else if...else

    # if condition1:
    #   indented statement block for first true condition
    # elif condition2:
    #   indented statement block for first true condition
    # elif condition3:
    #   indented statement block for first true condition
    # else:
    #   execute if all expressions above are false

    print("Python Calculator")
    # calculator
    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2: "))
    print("\nSuitable Operators: +, -, *, /, // (integer division, % (modulo operator), ** (power)")
    op = input("Enter operator: ")

    if op == "+":
        print(num1 + num2)
        # or...
        # print("{w} {x} {y} = {z}" format(w=num1, x=op, y=num2, z=num1 + num2))
    elif op == "-":
        print(num1 - num2)
    elif op == "*":
        print(num1 * num2)
    elif op == "/":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 / num2)
    elif op == "//":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 // num2)
    elif op == "%":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 % num2)
    elif op == "**":
        print(num1 ** num2)
        # or...
        print(pow(num1, num2))
    else:
        print("Incorrect operator!")